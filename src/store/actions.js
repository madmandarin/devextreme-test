import {
  MUTATE_ADD_CHART,
  MUTATE_SET_CHART_FROM_ID,
  CREATE_CHART,
} from './types';
import {
  getData,
} from '../api';

export default {
  [CREATE_CHART]({ commit, state }, { type, id }) {
    const unicId = state.charts.length;
    commit(MUTATE_ADD_CHART, { id: unicId, type: 'skeleton' });
    getData(id).then((res) => {
      commit(MUTATE_SET_CHART_FROM_ID, { id: unicId, type, data: res.data });
    });
  },
};
