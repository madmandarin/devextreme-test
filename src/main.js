import Vue from 'vue';
import Vuetify from 'vuetify';
import App from './App.vue';
import store from './store';
import 'vuetify/dist/vuetify.min.css';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';

Vue.config.productionTip = false;

Vue.use(Vuetify);

const vuetify = new Vuetify();

new Vue({
  vuetify,
  store,
  render: (h) => h(App),
}).$mount('#app');
