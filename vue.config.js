module.exports = {
  devServer: {
    proxy: {
      '/v3': {
        target: 'https://run.mocky.io/',
        changeOrigin: true,
      },
    },
  },
};
