// mutations
export const MUTATE_ADD_CHART = 'charts/MUTATE_ADD_CHART';
export const MUTATE_SET_CHART_FROM_ID = 'charts/MUTATE_SET_CHART_FROM_ID';

// actions
export const CREATE_CHART = 'charts/CREATE_CHART';
