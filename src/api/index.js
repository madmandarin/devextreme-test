import axios from 'axios';

// eslint-disable-next-line import/prefer-default-export
export function getData(id) {
  return axios.get(`/v3/${id}`);
}
