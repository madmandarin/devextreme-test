// import Vue from 'vue';
import {
  MUTATE_ADD_CHART,
  MUTATE_SET_CHART_FROM_ID,
} from './types';

export default {
  [MUTATE_ADD_CHART](state, payload) {
    state.charts.push(payload);
  },
  [MUTATE_SET_CHART_FROM_ID](state, payload) {
    state.charts = state.charts.map((value, num) => (num === payload.id ? payload : value));
  },
};
